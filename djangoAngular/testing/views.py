from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.views.generic import View
from random import sample
from json import dumps


class Testing(View):
    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, 'index.html')

class Data(View):
    def get(self, request, *args, **kwargs):
        x  =range(10)
        print(x)
        data = {'lista':sample(x,5)}
        return HttpResponse(dumps(data))
