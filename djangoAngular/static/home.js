var mi_app = angular.module('miApp', [],
    function(
        $interpolateProvider) {
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');

    });

mi_app.run(function($http) {
    $http.defaults.withCredentials = true;
    $http.defaults.useXDomain = true;
    $http.defaults.headers.common['X-CSRFToken'] = $(
        'meta[name="csrf-token"]').attr('content');
});


mi_app.controller('miController', ['$scope', '$http',
    function miController($scope, $http, alert) {
        $scope.list = [1,2,3];
         $scope.lastName= "joe";
         $scope.myCol = "c3c3c3"
        $scope.data = {
            csrfmiddlewaretoken: $('meta[name="csrf-token"]').attr('content')
        };

        $scope.remove = function(index,x){
            $scope.list.splice(index,1);
        };

        $scope.add = function(){
            var i= 2;
            while (true) {

                if($scope.list.indexOf($scope.list.length + i) == -1)
                {
                    $scope.list.push($scope.list.length + i);
                    break;
                }
                i+=1;
            }
        };



        $scope.load_data = function() {
            $http({
                url: ('/data/'),
                method: 'GET',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then($scope.success, $scope.error);
        };

        $scope.success = function(obj){
            $scope.list = obj.data.lista;
        };
        $scope.error = function(data){
            //error handle
        };


    }
]);
